/** @jsxImportSource @emotion/react */
import Logo from "../../assets/logo.png";
import Textfield from "../TextField";
import Checkbox from "../Checkbox";
import { css } from "@emotion/react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { Link } from "react-router-dom";
import { useMediaQuery } from "../../hooks/useMediaQuery";

function Login() {
  const isMobile = useMediaQuery("(max-width: 640px)");
  const link = css`
    color: #5850ec;
    opacity: 1;
    cursor: pointer;
    transition: 0.4s;
    &:hover {
      opacity: 0.9;
    }
  `;
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        width: "100vw",
        height: "100vh",
        gap: "1.4rem",
      }}
    >
      <img src={Logo} style={{ height: "48px", width: "165px" }} />
      <span
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <p
          style={{
            lineHeight: "2.25rem",
            fontSize: "1.875rem",
            fontWeight: "800",
          }}
        >
          Criar nova conta
        </p>
        <p
          style={{
            lineHeight: "1.25rem",
            fontSize: "1rem",
            fontWeight: "800",
            marginTop: "10px",
          }}
        >
          <span
            style={{
              color: "#7F8690",
              fontFamily: "initial",
              fontWeight: "500",
            }}
          >
            Ou
          </span>
          {"  "}
          <Link to="/login">
            <span
              css={link}
              style={{
                fontWeight: "500",
              }}
            >
              {" "}
              entrar na sua conta existente
            </span>
          </Link>
        </p>
      </span>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          width: isMobile ? "100vw" : "448px",
          background: "white",
          boxShadow: isMobile
            ? "none"
            : "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
          borderRadius: "0.5rem",
          marginTop: "10px",
        }}
      >
        <span
          style={{
            width: "82%",
            display: "flex",
            flexDirection: "column",
            gap: "1.5rem",
            padding: "2.3vh 0",
          }}
        >
          <Formik
            initialValues={{ email: "", password: "", repeat: "" }}
            validationSchema={Yup.object({
              email: Yup.string()
                .required("Required field")
                .email("Not a valid e-mail"),
              repeat: Yup.string()
                .required("Required field")
                .email("Not a valid e-mail"),
              password: Yup.string().required("Required field"),
              terms: Yup.boolean().required("Required field"),
            })}
          >
            <Form>
              <Textfield label="E-mail" name="email" />
              <Textfield label="Repetir e-mail" name="repeat" />
              <Textfield label="Senha" name="password" />
              <Checkbox
                label="Eu li e aceito os termos de uso, termos de licença de uso de software, política de conteúdo da Kiwify"
                name="terms"
              />
              <button
                css={css`
                  width: 100%;
                  height: 20px;
                  background: #5850ec;
                  height: 40px;
                  opacity: 1;
                  transition: 0.4s;
                  &:hover {
                    opacity: 0.9;
                  }
                `}
                type="submit"
              >
                <p
                  style={{
                    fontFamily: "inherit",
                    color: "white",
                    fontWeight: "500",
                    fontSize: "0.9rem",
                  }}
                >
                  Criar conta
                </p>
              </button>
            </Form>
          </Formik>
        </span>
      </div>
    </div>
  );
}

export default Login;
