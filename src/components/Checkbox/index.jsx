//----------------CORE-----------------//
import { memo } from "react";
import { FastField } from "formik";

const FormkikTextField = memo(({ name, label, ...props }) => {
  return (
    <FastField name={name}>
      {({ field, form, meta }) => {
        return (
          <div style={{ display: "flex", margin: "10px 0" }}>
            <input
              type="checkbox"
              name="vehicle1"
              {...field}
              {...props}
              style={{
                borderRadius: "0.375rem",
                border:
                  meta.touched && meta.error
                    ? "1px solid #F05555"
                    : "1px solid #E0E2E6",
                height: "23px",
                width: "23px",
                boxShadow: "0 1px 2px 0 rgb(0 0 0 / 5%)",
                position: "relative",
                bottom: "4px",
                cursor: "pointer",
              }}
            />
            <label
              style={{
                lineHeight: "1.25rem",
                fontSize: "0.875rem",
                fontWeight: "500",
                marginBottom: "10px",
                marginLeft: "10px",
              }}
            >
              {label}
            </label>
          </div>
        );
      }}
    </FastField>
  );
});

export default FormkikTextField;
