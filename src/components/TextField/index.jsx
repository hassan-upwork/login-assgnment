/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { memo } from "react";
import { FastField } from "formik";

const FormkikTextField = memo(({ name, label, ...props }) => {
  return (
    <FastField name={name}>
      {({ field, form, meta }) => {
        const style = css`
          border-radius: 0.375rem;
          border: ${meta.touched && meta.error
            ? "1px solid #F05555"
            : "1px solid #ccc"};
          height: 38px;
          padding: 10px;
          transition: 0.2s;
          box-shadow: 0 1px 2px 0 rgb(0 0 0 / 5%);
          transition: 0.3s;
          &:focus {
            box-shadow: 0 0 5px rgba(81, 203, 238, 1);
          }
        `;
        return (
          <span
            style={{
              display: "flex",
              flexDirection: "column",
              gap: "10px",
              marginBottom: "5px",
            }}
          >
            <label
              style={{
                position: "relative",
                top: "4px",
                fontSize: "0.85rem",
                fontWeight: "500",
              }}
            >
              {label}
            </label>
            <input {...field} {...props} type="text" css={style} />
            <p
              style={{
                lineHeight: "1.25rem",
                fontSize: "0.75rem",
                fontWeight: "500",
                color: "#F05555",
                position: "relative",
                bottom: "10px",
              }}
            >
              {meta.touched && meta.error ? meta.error : ""}
            </p>
          </span>
        );
      }}
    </FastField>
  );
});

export default FormkikTextField;
